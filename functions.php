<?php 
function load_wp_media_files(){
    wp_enqueue_media();
}
add_action('admin_enqueue_scripts', 'load_wp_media_files');

    //enable on dashboard featured image for entries
if ( function_exists( 'add_theme_support' ) ){
    add_theme_support( 'post-thumbnails' );
}

    //current uri
    function currentUri()
    {
        global $wp;
        $current_url = home_url(add_query_arg(array(), $wp->request));
        return (end(explode('/', $current_url)) == '' || end(explode('/', $current_url)) == 'cms') ? 'home' : end(explode('/', $current_url));
    }

    //current url
    function currentUrl()
    {
        global $wp;
        return home_url(add_query_arg(array(), $wp->request));
    }

    //get IMAGE id
    function get_image_id($image_url) {
        global $wpdb;
        $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )); 
        return @$attachment[0]; 
    }

    //get tags IMAGE
    function wp_get_attachment( $attachment_id ) {
        $attachment = get_post( $attachment_id );
        return array(
            'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
            'caption' => $attachment->post_excerpt,
            'description' => $attachment->post_content,
            'title' => $attachment->post_title
        );
    }

    //INFO
    function getInfoGeneral(){
        $arsg = array(
            'post_type'     => array('infogeneral'),
            'post_status'   => 'publish',
            'show_in_rest'  => true,
            'orderby'   => array(
                'menu_order'=>'ASC' 
            )
        );

        $query = new WP_Query($arsg);
        if ($query->have_posts()) {
            $data = array();
            while ($query->have_posts()) {
                $query->the_post(); 

                $data[] = array(
                    'id'                => get_the_id(),
                    'title'             => get_the_title(get_the_id()),
                    'slug'              => get_post_field('post_name'),
                    'permalink'             => get_permalink(get_post(get_the_id())),
                    'email'              => get_post_meta(get_the_id(),'email',true),
                    'tel'                => get_post_meta(get_the_id(),'tel',true),
                    'addres'             => get_post_meta(get_the_id(),'addres',true),
                    'ubication'          => get_post_meta(get_the_id(),'ubication',true),
                    'instagram'          => get_post_meta(get_the_id(),'instagram',true),
                    'twitter'            => get_post_meta(get_the_id(),'twitter',true),
                    'facebook'           => get_post_meta(get_the_id(),'facebook',true),
                    'youtube'            => get_post_meta(get_the_id(),'youtube',true),
                    'terms'              => wp_get_attachment_url(get_post_meta(get_the_id(),'terms',true))
                );
            }
            wp_reset_postdata();
        }
        return reset($data);
    }


    //word wrap
    function wrapText($string, $numMaxCaract)
    {
        if (strlen($string) <  $numMaxCaract) {
            $textWrap = $string;
        } else {
            $textWrap = substr($string, 0, $numMaxCaract);
            $ultimoEspacio = strripos($textWrap, " ");
            if ($ultimoEspacio !== false) {
                $textoCortadoTmp = substr($textWrap, 0, $ultimoEspacio);
                if (substr($textWrap, $ultimoEspacio)) {
                    $textoCortadoTmp .= '...';
                }
                $textWrap = $textoCortadoTmp;
            } elseif (substr($string, $numMaxCaract)) {
                $textWrap .= '...';
            }
        }
        return $textWrap;
    }

   
?>