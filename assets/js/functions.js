/*! modernizr 3.6.0 (Custom Build) | MIT *
* https://modernizr.com/download/?-backgroundblendmode-backgroundcliptext-backgroundsize-bgpositionshorthand-bgpositionxy-bgrepeatspace_bgrepeatround-bgsizecover-multiplebgs-setclasses !*/
!function(e,n,t){function r(e){var n=S.className,t=Modernizr._config.classPrefix||"";if(k&&(n=n.baseVal),Modernizr._config.enableJSClass){var r=new RegExp("(^|\\s)"+t+"no-js(\\s|$)");n=n.replace(r,"$1"+t+"js$2")}Modernizr._config.enableClasses&&(n+=" "+t+e.join(" "+t),k?S.className.baseVal=n:S.className=n)}function o(e,n){return typeof e===n}function s(){var e,n,t,r,s,i,a;for(var u in x)if(x.hasOwnProperty(u)){if(e=[],n=x[u],n.name&&(e.push(n.name.toLowerCase()),n.options&&n.options.aliases&&n.options.aliases.length))for(t=0;t<n.options.aliases.length;t++)e.push(n.options.aliases[t].toLowerCase());for(r=o(n.fn,"function")?n.fn():n.fn,s=0;s<e.length;s++)i=e[s],a=i.split("."),1===a.length?Modernizr[a[0]]=r:(!Modernizr[a[0]]||Modernizr[a[0]]instanceof Boolean||(Modernizr[a[0]]=new Boolean(Modernizr[a[0]])),Modernizr[a[0]][a[1]]=r),b.push((r?"":"no-")+a.join("-"))}}function i(){return"function"!=typeof n.createElement?n.createElement(arguments[0]):k?n.createElementNS.call(n,"http://www.w3.org/2000/svg",arguments[0]):n.createElement.apply(n,arguments)}function a(e){return e.replace(/([a-z])-([a-z])/g,function(e,n,t){return n+t.toUpperCase()}).replace(/^-/,"")}function u(e,n){return!!~(""+e).indexOf(n)}function l(e,n){return function(){return e.apply(n,arguments)}}function f(e,n,t){var r;for(var s in e)if(e[s]in n)return t===!1?e[s]:(r=n[e[s]],o(r,"function")?l(r,t||n):r);return!1}function d(e){return e.replace(/([A-Z])/g,function(e,n){return"-"+n.toLowerCase()}).replace(/^ms-/,"-ms-")}function c(n,t,r){var o;if("getComputedStyle"in e){o=getComputedStyle.call(e,n,t);var s=e.console;if(null!==o)r&&(o=o.getPropertyValue(r));else if(s){var i=s.error?"error":"log";s[i].call(s,"getComputedStyle returning null, its possible modernizr test results are inaccurate")}}else o=!t&&n.currentStyle&&n.currentStyle[r];return o}function p(){var e=n.body;return e||(e=i(k?"svg":"body"),e.fake=!0),e}function g(e,t,r,o){var s,a,u,l,f="modernizr",d=i("div"),c=p();if(parseInt(r,10))for(;r--;)u=i("div"),u.id=o?o[r]:f+(r+1),d.appendChild(u);return s=i("style"),s.type="text/css",s.id="s"+f,(c.fake?c:d).appendChild(s),c.appendChild(d),s.styleSheet?s.styleSheet.cssText=e:s.appendChild(n.createTextNode(e)),d.id=f,c.fake&&(c.style.background="",c.style.overflow="hidden",l=S.style.overflow,S.style.overflow="hidden",S.appendChild(c)),a=t(d,e),c.fake?(c.parentNode.removeChild(c),S.style.overflow=l,S.offsetHeight):d.parentNode.removeChild(d),!!a}function m(n,r){var o=n.length;if("CSS"in e&&"supports"in e.CSS){for(;o--;)if(e.CSS.supports(d(n[o]),r))return!0;return!1}if("CSSSupportsRule"in e){for(var s=[];o--;)s.push("("+d(n[o])+":"+r+")");return s=s.join(" or "),g("@supports ("+s+") { #modernizr { position: absolute; } }",function(e){return"absolute"==c(e,null,"position")})}return t}function v(e,n,r,s){function l(){d&&(delete P.style,delete P.modElem)}if(s=o(s,"undefined")?!1:s,!o(r,"undefined")){var f=m(e,r);if(!o(f,"undefined"))return f}for(var d,c,p,g,v,h=["modernizr","tspan","samp"];!P.style&&h.length;)d=!0,P.modElem=i(h.shift()),P.style=P.modElem.style;for(p=e.length,c=0;p>c;c++)if(g=e[c],v=P.style[g],u(g,"-")&&(g=a(g)),P.style[g]!==t){if(s||o(r,"undefined"))return l(),"pfx"==n?g:!0;try{P.style[g]=r}catch(y){}if(P.style[g]!=v)return l(),"pfx"==n?g:!0}return l(),!1}function h(e,n,t,r,s){var i=e.charAt(0).toUpperCase()+e.slice(1),a=(e+" "+w.join(i+" ")+i).split(" ");return o(n,"string")||o(n,"undefined")?v(a,n,r,s):(a=(e+" "+T.join(i+" ")+i).split(" "),f(a,n,t))}function y(e,n,r){return h(e,t,t,n,r)}var b=[],x=[],C={_version:"3.6.0",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,n){var t=this;setTimeout(function(){n(t[e])},0)},addTest:function(e,n,t){x.push({name:e,fn:n,options:t})},addAsyncTest:function(e){x.push({name:null,fn:e})}},Modernizr=function(){};Modernizr.prototype=C,Modernizr=new Modernizr;var S=n.documentElement,k="svg"===S.nodeName.toLowerCase();Modernizr.addTest("bgpositionshorthand",function(){var e=i("a"),n=e.style,t="right 10px bottom 10px";return n.cssText="background-position: "+t+";",n.backgroundPosition===t}),Modernizr.addTest("multiplebgs",function(){var e=i("a").style;return e.cssText="background:url(https://),url(https://),red url(https://)",/(url\s*\(.*?){3}/.test(e.background)});var _="Moz O ms Webkit",w=C._config.usePrefixes?_.split(" "):[];C._cssomPrefixes=w;var T=C._config.usePrefixes?_.toLowerCase().split(" "):[];C._domPrefixes=T;var z={elem:i("modernizr")};Modernizr._q.push(function(){delete z.elem});var P={style:z.elem.style};Modernizr._q.unshift(function(){delete P.style}),C.testAllProps=h,C.testAllProps=y,Modernizr.addTest("bgsizecover",y("backgroundSize","cover")),Modernizr.addTest("backgroundcliptext",function(){return y("backgroundClip","text")}),Modernizr.addTest("bgpositionxy",function(){return y("backgroundPositionX","3px",!0)&&y("backgroundPositionY","5px",!0)}),Modernizr.addTest("bgrepeatround",y("backgroundRepeat","round")),Modernizr.addTest("bgrepeatspace",y("backgroundRepeat","space")),Modernizr.addTest("backgroundsize",y("backgroundSize","100%",!0));var E=function(n){var r,o=prefixes.length,s=e.CSSRule;if("undefined"==typeof s)return t;if(!n)return!1;if(n=n.replace(/^@/,""),r=n.replace(/-/g,"_").toUpperCase()+"_RULE",r in s)return"@"+n;for(var i=0;o>i;i++){var a=prefixes[i],u=a.toUpperCase()+"_"+r;if(u in s)return"@-"+a.toLowerCase()+"-"+n}return!1};C.atRule=E;var N=C.prefixed=function(e,n,t){return 0===e.indexOf("@")?E(e):(-1!=e.indexOf("-")&&(e=a(e)),n?h(e,n,t):h(e,"pfx"))};Modernizr.addTest("backgroundblendmode",N("backgroundBlendMode","text")),s(),r(b),delete C.addTest,delete C.addAsyncTest;for(var R=0;R<Modernizr._q.length;R++)Modernizr._q[R]();e.Modernizr=Modernizr}(window,document);

//var window
var windowWidth=$(window).width();
var windowHeight=$(window).height();
$(".number").keyup(function () {
	this.value = (this.value + '').replace(/[^0-9]/g, '');
});


//owl carousel basic
function sliderPlay(){
	if( $('.slidernew').length ){
	//$.noConflict();
	$('.slidernew').each(function(){
		var thisObjct = $(this);
		navVar = ( thisObjct.attr('data-nav') == 'true') ? true : false;
		dotsVar = ( thisObjct.attr('data-dots') == 'true') ? true : false;
		autoHeightVar = ( thisObjct.attr('data-autoHeight') == 'true') ? true : false;
		autoplay = ( thisObjct.attr('data-autoplay') > 0) ? thisObjct.attr('data-autoplay') : 0;
		responsiveItems = thisObjct.attr('data-responsive').split(',');
		owlSlider = thisObjct.owlCarousel({
			autoplay: (( autoplay > 0 ) ? true : false),
			loop:thisObjct.children().length > 1,
			autoplayHoverPause:(( autoplay > 0 ) ? true : false),
			autoplayTimeout:autoplay,
			nav: (( thisObjct.children().length==1 ) ? false : navVar),
			dots: (( thisObjct.children().length==1 ) ? false : dotsVar),
			autoHeight:autoHeightVar,
	        navText:["",""],//http://apps.eky.hk/css-triangle-generator/
	        video:true,
	        //animateOut: 'fadeOut',
	        //animateIn: 'slideInUp',
	        marginLeft:0,
	        stagePaddingLeft:0,
	        smartSpeed:450,
	        responsive:{
	        	0:{
	        		items:parseInt(responsiveItems[0])
	        	},
	        	600:{
	        		items:parseInt(responsiveItems[1])
	        	},
	        	1000:{
	        		items:parseInt(responsiveItems[2])
	        	}
	        }
	    });

	    //skip autoplay setting On play videos
	    thisObjct.find('.fixed-video').on('click', function() {
	        //owlSlider.trigger('owl.stop');
	        //console.log('Stop!'); 
	        thisObjct.mouseleave(function(){
	        	thisObjct.trigger( "mouseover", function(){
	        		thisObjct.find('.owl-controls .owl-nav div').click(function(){
	        			thisObjct.trigger( "mouseout" );
	        		});
	        		
	        		thisObjct.find('.owl-dots div').click(function(){
	        			thisObjct.trigger( "mouseout" );
	        		});
	        	}); 
	        });
	    });
	});
}
}
sliderPlay();

//mutiple carousel's
function sliderTigre(){
	$('.slidernew').find('.prevI').click(function(){
		$(this).parent().parent().parent().parent().parent().parent().trigger('prev.owl.carousel');
	});

	$('.slidernew').find('.nextI').click(function(){
		$(this).parent().parent().parent().parent().parent().parent().trigger('next.owl.carousel');
	});
}
sliderTigre();


//responsive menu
$('.btnMenu').click(function(event) {
	event.preventDefault();
	open = $(this).data('open');
	if(open){
		$(this).find('#closeEsp').css({'display':'block'});
		$(this).find('#iconEsp').css({'display':'none'});
		$(this).data('open',false);
	}
	else{
		$(this).find('#closeEsp').css({'display':'none'});
		$(this).find('#iconEsp').css({'display':'block'});
		$(this).data('open',true);
	}
});

//viewport
var animations = [
{ className: '.left-movement-bounce', animationClass: 'bounceInLeft', hideFirst: true },
{ className: '.right-movement-bounce', animationClass: 'bounceInRight', hideFirst: true },
{ className: '.left-movement', animationClass: 'fadeInLeft', hideFirst: true },
{ className: '.right-movement', animationClass: 'fadeInRight', hideFirst: true },
{ className: '.up-movement', animationClass: 'fadeInUp', hideFirst: true },
{ className: '.down-movement', animationClass: 'fadeInDown', hideFirst: true },
{ className: '.rotate-movement', animationClass: 'flipInX', hideFirst: true },
{ className: '.tada-movement', animationClass: 'tada', hideFirst: true },
{ className: '.shake-movement', animationClass: 'shake', hideFirst: true }
];

function viewportAnimate(animations) {
    //console.log(animations.length);
    $.each(animations, function(i, v) {
    	var hideChecker = v.hideFirst;
    	var hidden;
    	if (hideChecker == true) { hidden = "hidden"; } else { hidden = ""; }
    	$(v.className).addClass(hidden).viewportChecker({
    		classToAdd: 'animated visible ' + v.animationClass,
    		offset: 180,
    		repeat: true
    	});
    });
}

viewportAnimate(animations);

function scrollToTop(scrollDuration) {
	var scrollStep = -window.scrollY / (scrollDuration / 15),
	scrollInterval = setInterval(function(){
		if ( window.scrollY != 0 ) {
			window.scrollBy( 0, scrollStep );
		}
		else clearInterval(scrollInterval); 
	},15);
}

//remove paremeter from url
function removeGet(){
	setTimeout(function() {
		window.history.pushState("", "", location.href.split('?')[0]);
	},10);
}


//set value checkbox
$('input[type=checkbox]').each(function(e){
	var obj = $(this);

	$.fn.hasAttr = function(name) {  
		return this.attr(name) !== undefined;
	};
	if( obj.hasAttr('checked') ){
		obj.attr('value','1');
	}else{
		obj.attr('value','');
	}
	obj.click(function(){
		var color = ( obj.attr('class') == 'black' ) ? '#333|#fff' : '#B69D74|#333' ;
		if( obj.prop('checked') || !obj.attr('value').length ){
			obj.attr('value','1');
            obj.css({'background':color.split('|')[0], 'box-shadow': 'inset 0 0 0 2px transparent', 'opacity':1});//optional
        }else{
        	obj.attr('value','');
            obj.css({'background':'#FFF', 'opacity':1});//optional
        }
    }); 
});


function sortNumber(a,b) {
	return a - b;
}

//detect direction event scroll
var lastScrollTop = 0;
$(window).scroll(function(event) {
	var st = $(this).scrollTop();
	if (st > 170) {
		if (st > lastScrollTop) {
            // console.log('down');
            if (!$('header').hasClass('toggle')) {
            	$('header').slideUp('fast');
            }
        } else {
            // console.log('up');
            $('header').slideDown('fast');
        }
        lastScrollTop = st;
    }
});
var newHeight = ($('header').height());
$('.breakHeader').css({'height':newHeight+'px'});


function autoHeight(el) {
	if ($(el).length) {
        box = [];
        $(el).each(function(e) {
            box.push($(this).outerHeight());
        });

        boxMaxHeight = (box.sort(sortNumber).pop() + 1);
        $(el).css({ 'height': (boxMaxHeight + 1) + 'px' });
    }
}

function itemimgAutoHeight(el){
	if( $(el).length){
		//set max height
		box = [];
		$(el).each(function(e){
			$(this).width() + '...' + $(this).outerWidth();
			box.push( $(this).outerWidth() );
		});
		boxMaxHeight = ( box.sort(sortNumber).pop() );
		//console.log('--'+boxMaxHeight+'--');
		$(el).css({'height':boxMaxHeight+'px'});
	}
}

itemimgAutoHeight('.box-staff');


/**btn**/
$(window).on("load", function() {
	$('button.navbar-toggler').addClass('false');
	$('button.navbar-toggler').click(function() {
		var button = $(this);
		if (button.attr('aria-expanded') == 'false') {
			button.removeClass('false');
			button.addClass('true');
			button.find('.open').addClass('d-none');
			button.find('.close').removeClass('d-none');
		} else {
			button.removeClass('true');
			button.addClass('false');
			button.find('.open').removeClass('d-none');
			button.find('.close').addClass('d-none');;
		}
	});
});

var owl = $('.owl-carousel');
owl.owlCarousel();
$('.customNextBtn').click(function() {
	owl.trigger('next.owl.carousel');
})


//Anclado SUAVE Y LENTO
$('.anclaSlow').click(function() {
	var destino = $(this.hash);
	if (destino.length == 0) {
		destino = $('a[name="' + this.hash.substr(1) + '"]');
	}
	if (destino.length == 0) {
		destino = $('html');
	}
	$('html, body').animate({ scrollTop: destino.offset().top }, 500);
	return false;
});
//plans
function number_format(number){
	return new Intl.NumberFormat().format(number);
}

$('.anclaSlow').each(function(e){
   $(this).click(function(){
   		$('.anclaSlow').each(function(){
   			$(this).removeClass('active');
   		});
   		$(this).addClass('active');
   });
});

// New Mailer
$(function () {
	$('form').on('submit', function (event) {
		if (!$(this).attr('data-mailreport')) {
			return;
		}
		event.preventDefault();
		idForm = $(this).attr('id');
		send_copy = ($(this).attr('data-sendcopy') != '' || typeof $(this).attr('data-sendcopy') != '') ? $(this).attr('data-sendcopy') : 'false';
		form = $(this);
		j = 0;
		fields = '';
		form.find('*').each(function () {
			obj = $(this);
			if (obj.prop('required')) {
				if (obj.val() == '' || typeof obj.val() == '') {
					fields += obj.attr('name') + ', ';
					j++;
				}
			}
		});

		var formData = new FormData();
		$("form#" + idForm + " :input").each(function () {
			var input = $(this);
			if (typeof input.attr('name') !== "undefined") {
				if (input.attr('type') === 'file') {
					formData.append(input.attr('name'), input[0].files[0]);
				} else {
					if (input.attr('type') === 'radio') {
						if (input.hasAttr('checked')) {
							formData.append(input.attr('name'), input.val());
						}
					} else {
						formData.append(input.attr('name'), input.val());
					}
				}
			}
		});

		if (j == 0) {
			for (var pair of formData.entries()) {
				console.log(pair[0] + ', ' + pair[1]);
			}
			$.ajax({
				type: "POST",
				url: baseUrl.replace('cms/', '') + 'mailreport/mail?section=' + idForm + '&pathUrl=' + baseUrl + '&pathTheme=' + baseUrl + '&send_copy=' + send_copy,
				//dataType: "json",
				data: formData,
				contentType: false,
				processData: false,
				cache: false,
				success: function (result) {
					//data = JSON.parse(result);
					// msg = (data.code=='200')?'<strong>Mensaje del sistema: </strong><br/>':'<strong>Error del sistema: </strong><br/>';
					swal("Perfecto!", "Pronto nos pondremos en contacto contigo.", "success");
					form[0].reset();
				},
				error: function (result) {
					//data = JSON.parse(result);
					// msg = (data.code=='200')?'<strong>Mensaje del sistema: </strong><br/>':'<strong>Error del sistema: </strong><br/>';
					swal("Error!", "Hay un error, vuelve a intentar", "error");
					form[0].reset();
				}
			});
		} else {
			//swal('<strong>Error!</strong><br/>Debe completar los campos:<br/>'+fields);
			swal("Existe un problema!", 'Debe completar los campos:<br/>' + fields, "info");
		}
	});
});


