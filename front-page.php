<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <meta name="author" content="Prueba">
    <meta name="Copyright" content="Copyright © <?php echo date('Y')?>| All Rights Reserved.">
    <meta name="DC.title" content="Prueba | WebPortal development by TheDot-Studio ">
    <meta name="DC.subject" content="Design and Development by TheDot-Studio">
    <meta name="DC.creator" content="Wordpress, Illustrator, SublimeText, Bootstrap, HTML5, JS, AJAX, CSS3.">

    <!-- Favicons-->
    <link rel="shortcut icon" sizes="16x16" href="<?php echo get_template_directory_uri()?>/assets/image/icon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri()?>/assets/image/icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri()?>/assets/image/icon.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri()?>/assets/image/icon.png">

    <title>Prueba | Inicio</title>

    <?php wp_head();?>

</head>
<body>


    <div class="container-full land">
        <div class="row">
            <div class="col-md-6">
                <img src="<?php echo get_template_directory_uri()?>/assets/image/dog.png" alt="Prueba" class="w-100 px-5">
            </div>
            <div class="col-md-6 form">
                <div class="row">
                    <?php  echo do_shortcode('[contact-form-7 id="5" title="form"]'); ?>
                </div>
            </div>
        </div>
    </div>



    
    <link href="<?php echo get_template_directory_uri()?>/assets/css/all.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/assets/css/base.css">

    <script>
        var pathUrl = '<?php echo get_site_url()?>/';
    </script>

    <!-- JavaScript -->
    <script src="<?php echo get_template_directory_uri()?>/assets/js/all.js"></script>
    <script src="<?php echo get_template_directory_uri()?>/assets/js/functions.js"></script>
</body>
</html>